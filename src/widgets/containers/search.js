import React, { Component } from 'react';
import Search from '../components/search';

export default class SearchContainer extends Component {
	state = {
		value : 'Prueba value'
	}
	handleSubmit = (event) =>{
		event.preventDefault();
		console.log(this.input.value)
	}
	setInputRef = element => {
		this.input = element
	}
	handleInputChange = element => {
		this.setState({
			value: element.target.value.replace(' ','.')
		})
	}
	render() {
		return (
			<Search 
				setRef={this.setInputRef} 
				handleChange={this.handleInputChange} 
				handleSubmit={this.handleSubmit}
				value={this.state.value}
			/>
		);
	}
}
