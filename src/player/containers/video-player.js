import React, { Component } from 'react';
import VideoPlayerLayout from '../components/video-layout';

export default class VideoPlayer extends Component {
	render() {
		return (
			<VideoPlayerLayout>
				<video 
					src="http://peach.themazzone.com/durian/movies/sintel-1024-surround.mp4" 
					controls 
					autoPlay 
				/>
			</VideoPlayerLayout>
		);
	}
}
