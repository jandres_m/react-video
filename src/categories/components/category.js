import React from 'react';
import PlayList from '../../playlist/playlist';
import './category.css';

function Category(props) {
	return(
		<div className="Category">
			<b className="Category-description">{ props.description }</b>
			<h2 className="Category-title">{ props.title }</h2>
			{
				<PlayList 
					playlist={props.playlist}
					handleOpenModal={props.handleOpenModal}
				/>
			}
		</div>
	)
}

export default Category;