import React, { PureComponent } from 'react';
import ProTypes from 'prop-types';
import './media.css';

export default class Media extends PureComponent {
	state = {
		author: this.props.author
	}
	// constructor(props){
	// 	super(props)
	// }
	// handleClick = (event) => {
	// 	this.setState({
	// 		author: 'Julian'
	// 	})
	// }
	render(){
		return (
			<div className="Media" onClick={this.props.handleClick}>
				<div className="Media-cover">
					<img
						src={this.props.cover}
						width={260}
            			height={160}
						className="Media-image"
					/>
					<h3 className="Media-title">{this.props.title} </h3>
					<b className="Media-author">{this.state.author}</b>
				</div>
			</div>
		)
	}
}


Media.propTypes = {
	title: ProTypes.string.isRequired,
	type: ProTypes.oneOf(['video','audio'])
}
