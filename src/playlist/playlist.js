import React from 'react';
import Media from './media';
import './playlist.css';

function PlayList(props) {
	// const playlist = props.data.categories
	return(
		<div>
			<div className="PlayList">
				{
					props.playlist.map((list) => {
						return <Media 
									{...list} key={list.id} 
									handleClick={props.handleOpenModal}
								/>
					})
				}
			</div>
		</div>
	)
}

export default PlayList;